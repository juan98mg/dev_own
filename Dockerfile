FROM tomcat:latest
ENV TZ=America/Bogota
ADD devco.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
CMD ["catalina.sh", "run"]