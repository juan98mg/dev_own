package com.devco.devops;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class Greeting {
    
    @GetMapping("/{name}")
    public String greeting(@PathVariable("name") String name){
        return "Hi "+name;
    }
}
