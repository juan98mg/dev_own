package com.devco.devops;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = DevopsApplication.class)
@AutoConfigureMockMvc
class DevopsApplicationTests {


	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void testHellow(){

		var response = restTemplate.getForEntity(createURLWithPortV1("juan"), String.class);
	
		assertEquals(HttpStatus.OK, response.getStatusCode(), "status code != 200");
		assertEquals("Hi juan", response.getBody());
		

	}


	private String createURLWithPortV1(String uri) {
		return "http://localhost:" +port+"/"+ uri;
	}




	public int getPort() {
		return this.port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public TestRestTemplate getRestTemplate() {
		return this.restTemplate;
	}

	public void setRestTemplate(TestRestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}


}
